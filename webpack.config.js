const path = require('path')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const OptimizeCssAssetsWebpackPlugin = require('optimize-css-assets-webpack-plugin')
const TerserWebpackPlugin = require('terser-webpack-plugin')
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer')
const is_dev = process.env.NODE_ENV === 'development'
const setFilename = ext => `[name].${ext}`
const fileOptimization = () => {
	const config = {
		splitChunks: {
			chunks: 'all'
		}
	}

	if (!is_dev) {
		config.minimizer = [
			new OptimizeCssAssetsWebpackPlugin(),
			new TerserWebpackPlugin()
		]
	}

	return config
}
const applyCssLoaders = loader => {
	const loaders = [{
		loader: MiniCssExtractPlugin.loader,
		options: {
			hmr: is_dev
		}
	}, 'css-loader']

	if (loader) loaders.push(loader)

	return loaders
}
const setBabelOptions = preset => {
	const options = {
		presets: [
			'@babel/preset-env'
		],

		plugins: [
			'@babel/plugin-proposal-class-properties'
		]
	}

	if (preset) options.presets.push(preset)

	return options
}
const setJsLoaders = () => {
	const loaders = [
		{
			loader: 'babel-loader',
			options: setBabelOptions()
		}
	]

	if (is_dev) loaders.push('eslint-loader')

	return loaders
}
const addPlugins = () => {
	const base_plugins = [
		new CleanWebpackPlugin(),

		new CopyWebpackPlugin({
			patterns: [
				{
					from: path.resolve(__dirname, 'src/assets/favicon.png'),
					to: path.resolve(__dirname, 'web/assets')
				},

				{
					from: path.resolve(__dirname, 'src/assets/slide_arrow.png'),
					to: path.resolve(__dirname, 'web/assets')
				},

				{
					from: path.resolve(__dirname, 'src/assets/form_flowers.jpg'),
					to: path.resolve(__dirname, 'web/assets')
				},

				{
					from: path.resolve(__dirname, 'src/assets/instagram.jpg'),
					to: path.resolve(__dirname, 'web/assets')
				},

				{
					from: path.resolve(__dirname, 'src/assets/popova.png'),
					to: path.resolve(__dirname, 'web/assets')
				}
			]
		}),

		new MiniCssExtractPlugin({
			filename: setFilename('css')
		})
	]

	if (!is_dev) base_plugins.push(new BundleAnalyzerPlugin())

	return base_plugins
}

module.exports = {
	context: path.resolve(__dirname, 'src'),

	entry: {
		main: ['@babel/polyfill', './index.js']
	},

	output: {
		filename: setFilename('js'),
		path: path.resolve(__dirname, 'web/assets')
	},

	resolve: {
		alias: {
			'@': path.resolve(__dirname, 'src'),
			'@models': path.resolve(__dirname, 'src/models'),
			'@assets': path.resolve(__dirname, 'src/assets'),
			'@styles': path.resolve(__dirname, 'src/styles')
		}
	},

	optimization: fileOptimization(),

	devServer: {
		port: 4200,
		hot: is_dev
	},

	devtool: is_dev ? 'source-map' : '',

	mode: 'development',

	plugins: addPlugins(),

	module: {
		rules: [
			{
				test: /\.css$/,
				use: applyCssLoaders()
			},

			{
				test: /\.s[ac]ss$/,
				use: applyCssLoaders('sass-loader')
			},

			{
				test: /\.(png|jpeg|jpg|svg|gif|webp)$/,
				use: ['file-loader']
			},

			{
				test: /\.(ttf|woff|woff2)$/,
				use: ['file-loader']
			},

			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: setJsLoaders()
			}
		]
	}
}