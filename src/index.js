import Glide, { Autoplay, Swipe, Controls, Breakpoints } from '@glidejs/glide/dist/glide.modular.esm'
import ScrollMagic from 'scrollmagic';
import objectFitImages from 'object-fit-images'
import '@glidejs/glide/dist/css/glide.core.css'
import '@styles/default.scss'
import '@styles/redactor.scss'
import '@styles/header.scss'
import '@styles/footer.scss'
import '@styles/hero.scss'
import '@styles/about.scss'
import '@styles/card.scss'
import '@styles/services.scss'
import '@styles/price.scss'
import '@styles/feedbacks.scss'
import '@styles/form.scss'
import '@styles/instagram.scss'

objectFitImages()

const controller = new ScrollMagic.Controller()

const break_sections = document.querySelectorAll('section')

const hero_slider_images = document.querySelectorAll('.hero_image')

const header = document.querySelector('.header')

const header_links = document.querySelectorAll('.header_nav_item')

const header_burger = document.querySelector('.header_mobile_burger')

const main_form = document.querySelector('#main_form')

const hero_slides = document.querySelector('.hero_slider')

const feedbacks_posts = document.querySelector('.feedbacks_slider')

const instagram_posts = document.querySelector('.instagram_slider')

const break_sections_classes = []

let client_width = document.documentElement.clientWidth

let last_scroll_top

window.onload = () => {
  if (localStorage.getItem('form_sended')) {
    const form_top = document.querySelector('#form').offsetTop
    setTimeout(() => window.scrollTo(0, form_top), 100)

    if (document.querySelector('.form_success_sended')) {
      localStorage.removeItem('form_sended')
    }
  }
}

if (hero_slides) {
  const glide_gero = new Glide('.hero_slider', {
    type: 'carousel',
    autoplay: 5000,
    animationDuration: 2000,
    animationTimingFunc: 'ease-out',
    hoverpause: false
  })

  glide_gero.mount({ Autoplay })
}

if (feedbacks_posts) {
  const glide_feedback = new Glide('.feedbacks_slider', {
    type: 'carousel',
    autoplay: false,
    animationDuration: 700,
    animationTimingFunc: 'cubic-bezier(0.215, 0.61, 0.355, 1)'
  })

  glide_feedback.mount({ Swipe, Controls })
}

if (instagram_posts) {
  const glide_instagram = new Glide('.instagram_slider', {
    type: 'carousel',
    autoplay: 2000,
    animationDuration: 700,
    animationTimingFunc: 'cubic-bezier(0.215, 0.61, 0.355, 1)',
    gap: 20,
    perView: 4,
    breakpoints: {
      767: {
        perView: 1,
        gap: 0
      },
      991: {
        perView: 2
      },
      1199: {
        perView: 3
      }
    }
  })

  glide_instagram.mount({ Autoplay, Swipe, Controls, Breakpoints })
}

header_links[0].classList.add('__active')

for (let i = 0; i < header_links.length; i++) {
  header_links[i].querySelector('a').addEventListener('click', switchMenuItems)
}

objectFitImages('img')

for (let i = 0; i < break_sections.length; i++) {
  break_sections_classes.push(break_sections[i].className)
}

break_sections_classes.forEach((section, index) => {
  const break_scene = new ScrollMagic.Scene({
    triggerElement: document.querySelector(`.${section}`),
    triggerHook: 0.75
  })
    .setClassToggle(`.${section}`, '__active')
    .on('enter leave', changeMenuActiveClass)
    .addTo(controller)
})

function changeMenuActiveClass(event) {
  const active_sections = document.querySelectorAll('section.__active')
  const active_section_id = active_sections[active_sections.length - 1].id
  const active_navlink_item = document.querySelector(`[href="#${active_section_id}"]`)

  if (active_navlink_item) {
    document.querySelector('li.__active').classList.remove('__active')
    active_navlink_item.parentNode.classList.add('__active')
  }
}

header_burger.addEventListener('click', toggleNavMenu)

window.addEventListener('resize', () => {
  client_width = document.documentElement.clientWidth
})

window.addEventListener('scroll', e => {
  const scroll_top = window.pageYOffset

  // if (scroll_top < document.querySelector('.hero_content').clientHeight) {
  //   const height_percent = 100 / (document.querySelector('.hero_content').clientHeight / scroll_top) / 100 / 8 + 1

  //   for (let i = 0; i < hero_slider_images.length; i++) {
  //     hero_slider_images[i].style.transform = `scale(${height_percent}) translate3d(0, ${scroll_top / 4}px, 0)`
  //   }
  // }

  if (scroll_top >= header.clientHeight) {

    if (scroll_top > last_scroll_top) {
      header.classList.add('__hidden')
    } else {
      header.classList.remove('__hidden')
    }
  }

  last_scroll_top = scroll_top
})

function switchMenuItems(e) {
  e.preventDefault()

  document.querySelector('li.__active').classList.remove('__active')
  e.target.parentNode.classList.add('__active')

  const anchor_dest = document.querySelector(`${e.target.hash}`)

  if (client_width < 1200) {
    header_burger.classList.remove('__active')
    header.classList.remove('__menu_active')
    setTimeout(() => anchor_dest.scrollIntoView({ behavior: 'smooth', block: 'start' }), 200)
  } else {
    anchor_dest.scrollIntoView({ behavior: 'smooth', block: 'start' })
  }
}

function toggleNavMenu(e) {
  if (e.currentTarget.classList.contains('__active')) {
    e.currentTarget.classList.remove('__active')
    header.classList.remove('__menu_active')
  } else {
    e.currentTarget.classList.add('__active')
    header.classList.add('__menu_active')
  }
}

main_form.onsubmit = e => localStorage.setItem('form_sended', true)